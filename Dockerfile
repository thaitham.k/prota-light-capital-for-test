FROM python:3.8.5-slim
WORKDIR /src
ADD ./src /src

RUN pip install -r requirements.txt
CMD python app.py