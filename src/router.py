from sys import flags
import time
from aiohttp import web
import core_function as coreFN

#use to check params before send in coreFN
def checkParameter(symbol,since,to,side = "buy"):
    if symbol not in coreFN.list_state_of_symbol:
        return {"success":False,"message":'this symbol not run in task'}

    if symbol in coreFN.list_state_of_symbol and not coreFN.list_state_of_symbol[symbol]["status"]:
        return {"success":False,"message":'this symbol '+coreFN.list_state_of_symbol[symbol]["message"]}

    if not since.isdecimal():
        return {"success":False,"message": 'params {since} in only contain int'}

    if not to.isdecimal():
        return {"success":False,"message": 'params {to} in only contain int'}

    if side not in ["buy","sell"]:
        return {"success":False,"message": 'params {side} is enum "buy" or "side"'}

    return {"success":True}



routes = web.RouteTableDef()


#=================================================
# (GET): get raw trade data
# (PARAMS): symbol,since,to
#=================================================
@routes.get('/get_raw_trades')
async def get_raw_trades(request):
    t0 = time.time()

    params_symbol = request.rel_url.query['symbol']
    params_since = request.rel_url.query['since']
    params_to = request.rel_url.query['to']

    check_result =  checkParameter(params_symbol,params_since,params_to)
    if(not check_result["success"]):
        return web.json_response(check_result)
    else:
        list_search = await coreFN.HttpAction().get_raw_trades(params_symbol,params_since,params_to)
        return web.json_response({"time_execute(s)": time.time() - t0 ,"data":list_search})

#=================================================
# (GET): get trades stat
# (PARAMS): symbol,since,to,side
#=================================================
@routes.get('/get_trades_stat')
async def get_trades_stat(request):
    t0 = time.time()
    
    params_symbol = request.rel_url.query['symbol']
    params_since = request.rel_url.query['since']
    params_to = request.rel_url.query['to']
    params_side = request.rel_url.query['side']

    check_result = checkParameter(params_symbol,params_since,params_to,params_side)
    if(not check_result["success"]):
        return web.json_response(check_result)
    else:
        dict_calculate = await coreFN.HttpAction().get_trades_stat(params_symbol,params_since,params_to,params_side)
        return web.json_response(dict({"time_execute(s)": time.time() - t0,"symbol":params_symbol,"time_from":params_since,"time_to":params_to,"side":params_side} , **dict_calculate))

#=================================================
# (GET): get volume imbalance
# (PARAMS): symbol,since,to
#=================================================
@routes.get('/get_volume_imbalance')
async def get_volume_imbalance(request):
    t0 = time.time()

    params_symbol = request.rel_url.query['symbol']
    params_since = request.rel_url.query['since']
    params_to = request.rel_url.query['to']

    check_result = checkParameter(params_symbol,params_since,params_to)
    if(not check_result["success"]):
        return web.json_response(check_result)
    else:
        dict_calculate = await coreFN.HttpAction().get_volume_imbalance(params_symbol,params_since,params_to)
        return web.json_response(dict({"time_execute(s)": time.time() - t0,"symbol":params_symbol,"time_from":params_since,"time_to":params_to} , **dict_calculate))

#=================================================
# (GET): get list status of symbol running
#=================================================
@routes.get('/get_list_trade_symbol')
async def get_list_trade_symbol(request):
    return web.json_response(coreFN.list_state_of_symbol)

#=================================================
# (POST): create new symbol in websocket
# (BODY): symbol
#=================================================
@routes.post('/subscribe_trade_symbol')
async def subscribe_trade_symbol(request):
    body = await request.json()
    dict_response = await coreFN.HttpAction().post_subscribe_trade_symbol(body)
    return web.json_response(dict_response)
