from datetime import datetime,timedelta
from symtable import Symbol
import websockets
import json 
import rejson
from redisearch.client import IndexType
from redisearch import Client, IndexDefinition, TextField , NumericField , Query 
from aiohttp import ClientSession
import asyncio



#global variable
list_state_of_symbol = {} #store status of symbol 

#สร้าง connect redis  
redis_for_raw_trades = Client(index_name="raw_trades",host='redis',port=6379, password='',decode_responses=True) #redis/search client
redis_json_for_raw_trades = rejson.Client(host='redis', port=6379, decode_responses=True) #redis/json client


#=============================================================================================================
# (class): This class provides utility functions to create task for get and store data trade raw
# 1. websocket_processing(self): create new symbol task run loop websocket  
# 2. prepare_data(self,dataTrade): raw trade data from binance format to correct formant and store redis
# 3. checkExist(self): check symbol exist in binance 
#==============================================================================================================
class createSymbol:
    def __init__(self,symbolDefault: str):
        self.symbolBinance = symbolDefault.replace("/","").lower()
        self.symbolDefault = symbolDefault

    #สร้างเว็บ websocket เเละ store ข้อมูลลง redis ผ่าน def prepare_data
    async def websocket_processing(self) -> None:
        #=================================================
        # create schema for index and search
        #=================================================
        SCHEMA = (
            NumericField("$.traded_at",sortable=True ,as_name="traded_at"),
            TextField("$.symbol",sortable=True ,as_name="symbol"),
            TextField("$.volume"),
            TextField("$.price"),
            TextField("$.side",sortable=True ,as_name="side"),
        )

        definition = IndexDefinition(index_type=IndexType.JSON)
        try:
            redis_for_raw_trades.create_index(SCHEMA, definition=definition)
        except Exception as e:
            print(e)


        #=================================================
        # create websocket to tricker store to redis
        #=================================================
        async with websockets.connect('wss://stream.binance.com:9443/stream?streams='+self.symbolBinance+'@trade') as websocket:
            list_state_of_symbol[self.symbolDefault] = {
                "status": True,
                "message": "Running"
            }
            while True:
                try:
                    message = await websocket.recv()
                    await self.prepare_data(json.loads(message))
                except websockets.exceptions.ConnectionClosed:
                    
                    list_state_of_symbol[self.symbolDefault] = {
                        "status": False,
                        "message": "connection websocket close please new subscribe"
                    }

                    break

    #format data and add to redis
    async def prepare_data(self,dataTrade: dict) -> None:
        #=================================================
        # prepare and store data
        #=================================================
        key = self.symbolDefault+":"+str(dataTrade["data"]["T"])
        redis_json_for_raw_trades.jsonset(key, rejson.Path.rootPath(), {
            "traded_at": int(dataTrade["data"]["T"]), # trade timestamp in unix milliseconds
            "symbol": self.symbolDefault, # trade symbol
            "volume": dataTrade["data"]["q"], # trade volume
            "price": dataTrade["data"]["p"],  # trade price
            "side": "buy" if dataTrade["data"]["m"] else "sell" # trade side
        })

        #=================================================
        # set time expire to key
        #=================================================

        #เวลาสำหรับหมดอายุ time_shift = traded_at(convert ms => s) then shift 5 min 
        time_shift = datetime.fromtimestamp( int(dataTrade["data"]["T"]) / 1000 ) +  timedelta(minutes=5)
        time_shift_covert_to_stamp = int(datetime.timestamp(time_shift)) # output => unix second

        #set เวลา หมดอายุ
        redis_for_raw_trades.redis.expireat(key,time_shift_covert_to_stamp)

    #check symbol exist in binance 
    async def checkExist(self):
      async with ClientSession() as session:
            async with session.get('https://api.binance.com/api/v3/exchangeInfo?symbol='+self.symbolBinance.upper()) as resp:
                data = await resp.json()
                if("timezone" in data):
                    return True
                else:
                    return False



#=============================================================================================================
# (class): This class provides functions to http action for response back
#==============================================================================================================
class HttpAction:
    #get raw trades data
    async def get_raw_trades(self,symbolDefault: str , since : str , to : str) -> list:
        #=================================================
        # prepare query data  and search
        #=================================================
        list_for_return = []
        paramsSymbol = "@symbol:"+symbolDefault
        paramsTime = "@traded_at:[{} {}]".format((since),(to))
        key_search = redis_for_raw_trades.search( Query(paramsTime + " "+ paramsSymbol).verbatim().paging(0, 1000000) )

        #=================================================
        # format data prepare to return
        #=================================================
        for realKey in key_search.docs:
            try:
                data_trade =  json.loads(realKey.json)
                data_trade["traded_at"] = str(data_trade["traded_at"])
                list_for_return.append(data_trade)
            except Exception as e:
                #time expire and delete
                pass
            
        return list_for_return

    #get trade stat
    async def get_trades_stat(self,symbolDefault: str , since : str , to : str ,side: str) -> dict:
        #=================================================
        # prepare query data  and search
        #=================================================
        paramsSymbol = "@symbol:"+symbolDefault
        paramsTime = "@traded_at:[{} {}]".format((since),(to))
        paramsSide = "@side:{}".format(side)    
        key_search = redis_for_raw_trades.search( Query(paramsSide +" "+ paramsTime + " "+ paramsSymbol).paging(0, 1000000) )

        #=================================================
        # format data prepare to return
        #=================================================
        total_side_spend = 0
        total_side_volume = 0
        for realKey in key_search.docs:
            try:
                data_trade =  json.loads(realKey.json)
                total_side_spend += float(data_trade["volume"]) * float(data_trade["price"])
                total_side_volume += float(data_trade["volume"])
            except Exception as e:
                #time expire and delete
                pass
        return { "volume": str(total_side_volume), "weight_avg_price": str(total_side_spend/total_side_volume if total_side_volume > 0 else 0) }

    #get volume imbalance
    async def get_volume_imbalance(self,symbolDefault: str , since : str , to : str) -> list:
        #=================================================
        # prepare query data  and search
        #=================================================
        paramsSymbol = "@symbol:"+symbolDefault
        paramsTime = "@traded_at:[{} {}]".format((since),(to))
        key_search = redis_for_raw_trades.search( Query(paramsTime + " "+ paramsSymbol).verbatim().paging(0, 1000000) )

        #=================================================
        # format data prepare to return
        #=================================================
        total_buy_volume = 0
        total_sell_volume = 0
        for realKey in key_search.docs:
            try:
                data_trade =  json.loads(realKey.json)
                if(data_trade["side"] == "buy"):
                    total_buy_volume += float(data_trade["volume"])
                else:
                    total_sell_volume += float(data_trade["volume"])
            except Exception as e:
                #expire and delete
                pass
        return {"volume_imbalance": total_buy_volume/(total_buy_volume + total_sell_volume) - 0.5 if(total_buy_volume + total_sell_volume) > 0 else 0 }
    
    #add symbol to websocket task
    async def post_subscribe_trade_symbol(self,body:dict) -> dict:
        if("symbol" in body):
            symbol = body["symbol"]
            object_symbol = createSymbol(symbol)
            if(await object_symbol.checkExist()):
                if (symbol not in  list_state_of_symbol) or (symbol in list_state_of_symbol and not list_state_of_symbol[symbol]["status"]):
                    asyncio.create_task(object_symbol.websocket_processing())
                return {"success":True}
            else:
                return {"success":False,"message":"no symbol"}
        else:
            return {"success":False,"message":"no symbol"}

    
if __name__ == "__main__":
    pass