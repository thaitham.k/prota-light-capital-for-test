import asyncio
from aiohttp import web
import core_function as coreFN
from router import routes 

async def main():
    #=================================================
    # create the application, as before
    #=================================================

    app = web.Application()
    app.add_routes(routes)

    #=================================================
    # add some tasks into the current event loop
    #=================================================
    
    #delete all db first time start
    try:
        coreFN.redis_for_raw_trades.redis.flushdb()
        coreFN.redis_for_raw_trades.drop_index("raw_trades")
    except Exception as e:
        pass

    #default start symbol BTC/USDT , ETH/USDT
    asyncio.create_task(coreFN.createSymbol("BTC/USDT").websocket_processing())
    asyncio.create_task(coreFN.createSymbol("ETH/USDT").websocket_processing())

    #=================================================
    # set up the web server
    #=================================================

    runner = web.AppRunner(app)
    await runner.setup()
    await web.TCPSite(runner,"0.0.0.0",5000).start()
    # wait forever, running both the web server and the tasks
    await asyncio.Event().wait()

asyncio.run(main())




   

 



    
